const slides = document.getElementsByClassName("image-to-show");
let slideIndex = 0;

function showSlides() {
  for (let i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;

  if (slideIndex > slides.length) {
    slideIndex = 1;
  }
  slides[slideIndex - 1].style.display = "block";
}

let do_showSlides = setInterval(showSlides, 3000);

function showSlides_start() {
  do_showSlides = setInterval(showSlides, 3000);
}
function showSlides_pause() {
  do_showSlides = clearInterval(do_showSlides);
}

document
  .getElementById("button-pause")
  .addEventListener("click", showSlides_pause);

document
  .getElementById("button-resume")
  .addEventListener("click", showSlides_start);
